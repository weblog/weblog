"""
Django settings for mysite project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from unipath import Path
import json


BASE_DIR = Path(__file__).absolute().ancestor(2)

try:
    with open(BASE_DIR.child('secrets.json')) as handle:
        SECRETS = json.load(handle)
except IOError:
    SECRETS = {}

DEBUG = SECRETS.get('debug', False)
TEMPLATE_DEBUG = SECRETS.get('debug', False)

ALLOWED_HOSTS = SECRETS.get('allowed_hosts', [])

SERVER = str(SECRETS.get('server', "http://127.0.0.1:8000"))

# Database Settings
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': SECRETS.get('db_name', 'name'),
        'USER': SECRETS.get('db_user', 'root'),
        'PASSWORD': SECRETS.get('db_password', 'password'),
        'HOST': SECRETS.get('db_host', '127.0.0.1'),
    }
}
SECRET_KEY = SECRETS.get('secret_key', '')


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'picker',
    'blog',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

PICKER_INSTALLED_APPS = (
    'jquery',
    'bootstrap',
)

ROOT_URLCONF = 'mysite.urls'

WSGI_APPLICATION = 'mysite.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/
TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATICFILES_DIRS = [BASE_DIR.child('static')]
