from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$', 'blog.views.index', name='index'),
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^tag/(?P<TAG>\w+)', 'blog.views.tag', name='tag'),
    url(r'^tags/', 'blog.views.tags', name='tags'),
    url(r'^categories/$', 'blog.views.categories', name='categories'),
    url(r'^categories/(?P<category_name>\w+)/$',
        'blog.views.category', name='category'),
    url(r'^categories/(?P<category_name>\w+)/(?P<num>[0-9]+)',
        'blog.views.article_page', name='article_page'),
    url(r'^admin/', include(admin.site.urls)),
)
