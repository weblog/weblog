from django.shortcuts import render
from django.http import HttpResponse
from blog.models import *
import picker


def context_title(data, title):
    data['title'] = title


def context_menu(data):
    category_obj = Category.get_all_categories()
    index_of_cate = [[index.id, index.name] for index in category_obj]
    data['menu'] = index_of_cate


def index(request):
    data = {}

    context_title(data, 'Weblog | Home')
    context_menu(data)

    recent_articles = list(Article.get_recent_articles(10))
    for recent_article in recent_articles:
        recent_article.content = recent_article.content[:100] + '...'

    data['recent_articles'] = recent_articles

    return render(request, 'index.html', data)


def article_page(request, num, category_name):
    data = dict()
    context_title(data, 'Weblog | Category ' + category_name)
    context_menu(data)
    article_obj = Article.get_article_by_id(int(num), category_name)

    data['title'] = article_obj.title
    data['content'] = article_obj.content
    data['category'] = article_obj.category
    data['tags'] = article_obj.tags.all()
    data['create_on'] = article_obj.create_on

    return render(request, 'display_article.html', data)


def categories(request):
    data = {}

    context_title(data, 'Weblog | Category')
    context_menu(data)

    category_obj = Category.get_all_categories()
    data['categories'] = category_obj

    return render(request, 'categories.html', data)


def category(request, category_name):
    data = {}

    context_title(data, 'Weblog | Category ' + category_name)
    context_menu(data)

    article_obj = Article.get_articles_by_category_name(category_name)

    data['articles'] = article_obj

    return render(request, 'category.html', data)


def tags(request):
    data = {}

    context_title(data, 'Weblog | Tags')
    context_menu(data)

    data['TAGS'] = [i for i in list(Tag.objects.all())]
    return render(request, 'tags.html', data)


def tag(request, TAG):
    data = {}

    context_title(data, 'Weblog | Tag ' + TAG)
    context_menu(data)

    data["TAG"] = TAG
    data["article_from_tag"] = [(i.id, i.category.name, i.title, i.content) for i in Article.objects.filter(tags__name=TAG)]
    return render(request, 'tag.html', data)
