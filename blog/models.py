from django.db import models
from django.http import Http404


# Create your models here.
class Article(models.Model):

    def __unicode__(self):
        return self.title

    @staticmethod
    def get_article_by_id(art_id, category_name):
        result = Article.objects.filter(pk=art_id,
                                        category__name__exact=category_name).all()
        if not result:
            raise Http404
        return result[0]

    @staticmethod
    def get_articles_by_category_name(category_name):
        param = {'category__name__iexact': category_name}
        results = Article.objects.filter(**param).all()
        if not results:
            raise Http404
        return results

    @staticmethod
    def get_all_articles():
        return Article.objects.all()

    @staticmethod
    def get_recent_articles(number):
        return Article.objects.all().order_by('-create_on')[:number]

    title = models.CharField(max_length=255)
    content = models.TextField()
    create_on = models.DateTimeField(auto_now_add=True)
    category = models.ForeignKey('Category')
    tags = models.ManyToManyField('Tag')


class Category(models.Model):

    def __unicode__(self):
        return self.name

    @staticmethod
    def get_all_categories():
        return Category.objects.all()

    @staticmethod
    def get_category_by_name(name):
        return Category.objects.filter(category__name__iexact=name).all()

    name = models.CharField(max_length=255)


class Tag(models.Model):

    def __unicode__(self):
        return self.name

    name = models.CharField(max_length=255)
